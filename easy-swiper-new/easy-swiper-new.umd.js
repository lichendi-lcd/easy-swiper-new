(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["easy-swiper-new"] = factory();
	else
		root["easy-swiper-new"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "2046":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (false) { var getCurrentScript; }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"0b6073b3-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--5!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./src/package/Swiper/index.vue?vue&type=template&id=004f481e&
var render = function render(){var _vm=this,_c=_vm._self._c;return _c('div',{staticClass:"swiper-container",style:(_vm.wrapStyle)},[_c('div',{class:['swiper-wrapper', _vm.ranStr]},[_vm._t("default")],2),_vm._t("thumbs"),_vm._t("tabTitle"),_vm._t("pagination")],2)
}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/package/Swiper/index.vue?vue&type=template&id=004f481e&

// CONCATENATED MODULE: ./src/package/EasySwiper.js
/* eslint-disable no-unused-vars */
var $block = {}

/**
 * @constructor
 * @desc 全屏卡片 基础样式
 */
function CardScreen(opt) {
  this.sign = 'cardScreen'
  this.touchMoveFn = undefined
  this.swiperFn = undefined
}

/**
 * @constructor
 * @desc 普通3d轮播
 */
function Coverflow(opt) {
  this.sign = 'coverflow'
  this.touchMoveFn = function () {
    var glideDirect, fingerSlideVal, relateFingerSlide, curSlideCenterOffset
    // var deep =
    //   (EasySwiper.adaptaivePx(this.spaceBetween, true) + this.curSlideWidth) /
    //   this.curSlideWidth; //比值
    var deep = this.touchRatio
    ;(glideDirect = this.swiperDirectionVal()), //左边是1 ，右边是 -1
      (fingerSlideVal = this.moveX - this.preX), //手指滑动的距离
      (relateFingerSlide = deep * fingerSlideVal), //手指滑动的相对距离
      (curSlideCenterOffset = this.offsetVal()) //当前居中滑块偏移量
    // 手指滑动的相对距离大于轮播的宽度时，抛出canContinuitySlide
    if (
      Math.abs(fingerSlideVal) > this.curSlideWidth &&
      this.canContinuitySlide
    ) {
      this.canSlide = false
      return false
    }
    this.wrapperDom.style.transform = `translate(${
      curSlideCenterOffset + relateFingerSlide
    }px)`
    this.wrapperDom.style.perspective = `${this.perspective}px`
    this.wrapperDom.style.perspectiveOrigin = `${
      Math.abs(curSlideCenterOffset - this.clientWidth / 2) - relateFingerSlide
    }px ${this.perspectiveOriginY}`

    this.traArray(this.getEle(`${this.slideClassArr}`)).forEach(
      (item, index, items) => {
        var distance = Math.abs(relateFingerSlide)
        var desc =
          distance >= Math.abs(this.curSildeNth - index) * this.curSlideWidth
            ? -1
            : 1
        var customOffsetY = this.customOffsetY
        var descY =
          distance / this.curSlideWidth > 1 ? 1 : distance / this.curSlideWidth
        if (this.swiperDirectionVal() === 1) {
          //向左滑动
          if (this.curSildeNth < index) {
            //小于中间
            item.style.transform = `translate3d(0px,${
              Math.abs(this.curSildeNth - index) * customOffsetY -
              descY * customOffsetY
            }px,${
              desc *
              (-Math.abs(this.curSildeNth - index) * this.curSlideWidth +
                distance)
            }px) rotateY(-${
              Math.abs(this.curSildeNth - index) * this.rotateDeg -
              descY * this.rotateDeg
            }deg)`
          } else {
            item.style.transform = `translate3d(0px,${
              Math.abs(this.curSildeNth - index) * customOffsetY +
              descY * customOffsetY
            }px,${
              -Math.abs(this.curSildeNth - index) * this.curSlideWidth -
              distance
            }px) rotateY(${
              Math.abs(this.curSildeNth - index) * this.rotateDeg +
              descY * this.rotateDeg
            }deg)`
          }
        } else {
          //向右滑动
          if (this.curSildeNth <= index) {
            item.style.transform = `translate3d(0px,${
              Math.abs(this.curSildeNth - index) * customOffsetY +
              descY * customOffsetY
            }px,${
              -Math.abs(this.curSildeNth - index) * this.curSlideWidth -
              distance
            }px) rotateY(-${
              Math.abs(this.curSildeNth - index) * this.rotateDeg +
              descY * this.rotateDeg
            }deg)`
            //
          } else {
            item.style.transform = `translate3d(0px,${
              Math.abs(this.curSildeNth - index) * customOffsetY -
              descY * customOffsetY
            }px,${
              desc *
              (-Math.abs(this.curSildeNth - index) * this.curSlideWidth +
                distance)
            }px) rotateY(${
              Math.abs(this.curSildeNth - index) * this.rotateDeg -
              descY * this.rotateDeg
            }deg)`
          }
        }
      }
    )
  }

  this.swiperFn = function (time) {
    const leftOrigin = this.offsetVal() - this.clientWidth / 2
    const customOffsetY = this.customOffsetY
    this.removeClsName(
      this.traArray(this.getEle(`${this.slideClassArr}`)),
      'swiper-slide-active'
    )
    this.traArray(this.getEle(`${this.slideClassArr}`)).forEach(
      (item, index, items) => {
        let degIndex = this.curSildeNth - index
        item.style.transitionDuration = time + 'ms'
        ;(item.style.transform = `translate3d(0px,
          ${Math.abs(this.curSildeNth - index) * customOffsetY}px,
          ${
            -Math.abs(this.curSildeNth - index) * this.curSlideWidth
          }px)  rotateY(${degIndex * this.rotateDeg}deg)`),
          (item.style.zIndex = -Math.abs(this.curSildeNth - index))
        if (Math.abs(this.curSildeNth - index) == 0) {
          this.setClsName(item, 'swiper-slide-active')
        }
      }
    )

    this.wrapperDom.style.perspective = `${this.perspective}px`
    this.wrapperDom.style.perspectiveOrigin = `${Math.abs(leftOrigin)}px ${
      this.perspectiveOriginY
    }`
  }
}

/**
 * @constructor
 * @desc tab轮播
 */
function Tabflow(opt) {
  this.sign = 'tabflow'
  this.touchMoveFn = undefined
  this.swiperFn = undefined
}

/**
 * @constructor
 * @desc 泡泡轮播
 */
function Bubble(opt) {
  this.sign = 'bubble'
  this.swiperFn = function (time) {
    const leftOrigin = this.offsetVal() - this.clientWidth / 2
    const bubbleScale = opt.bubbleScale
    const bubbleY = opt.bubbleY
    const bubbleBetween = opt.bubbleBetween
    const bubbleNum = opt.bubbleNum
    this.removeClsName(
      this.traArray(this.getEle(`${this.slideClassArr}`)),
      'swiper-slide-active'
    )
    this.traArray(this.getEle(`${this.slideClassArr}`)).forEach(
      (item, index, items) => {
        let curSildeNth = Math.abs(this.curSildeNth - index)
        let bubbleX =
          this.curSildeNth - index > 0 ? -bubbleBetween : bubbleBetween //由于透视值补充的间距值
        let scaleIndex = bubbleNum ? curSildeNth % 2 : curSildeNth
        item.style.transitionDuration = time + 'ms'
        ;(item.style.transform = `translate3d(${
          scaleIndex == 0 ? 0 : bubbleX
        }px,${scaleIndex == 0 ? 0 : bubbleY}px,0) scale(${
          scaleIndex == 0 ? 1 : bubbleScale
        })`),
          // (item.style.transform = `translate3d(${scaleIndex==0?0:bubbleX}px,${scaleIndex==0?0:bubbleY}px,-${scaleIndex==0?0:bubbleScale}px)`),
          (item.style.zIndex = -curSildeNth)
        if (Math.abs(this.curSildeNth - index) == 0) {
          this.setClsName(item, 'swiper-slide-active')
        }
      }
    )
    this.wrapperDom.style.perspective = `${this.perspective}px`
    this.wrapperDom.style.perspectiveOrigin = `${Math.abs(leftOrigin)}px ${
      this.perspectiveOriginY
    }`
  }
  this.touchMoveFn = function () {
    var glideDirect, fingerSlideVal, relateFingerSlide, curSlideCenterOffset
    var deep = this.touchRatio
    var bubbleNum = opt.bubbleNum
    var bubbleY = opt.bubbleY
    var bubbleBetween = opt.bubbleBetween
    var bubbleScale = opt.bubbleScale
    ;(glideDirect = this.swiperDirectionVal()), //左边是1 ，右边是 -1
      (fingerSlideVal = this.moveX - this.preX), //手指滑动的距离
      (relateFingerSlide = deep * fingerSlideVal), //手指滑动的相对距离
      (curSlideCenterOffset = this.offsetVal()) //当前居中滑块偏移量
    this.wrapperDom.style.transform = `translate(${
      curSlideCenterOffset + relateFingerSlide
    }px)`
    this.wrapperDom.style.perspective = `${this.perspective}px`
    this.wrapperDom.style.perspectiveOrigin = `${
      Math.abs(curSlideCenterOffset - this.clientWidth / 2) - relateFingerSlide
    }px ${this.perspectiveOriginY}`
    // 手指滑动的相对距离大于轮播的宽度时，抛出canContinuitySlide
    if (
      Math.abs(relateFingerSlide) > this.curSlideWidth &&
      this.canContinuitySlide
    ) {
      this.canSlide = false
      return false
    }
    var distance = Math.abs(relateFingerSlide)
    var descY =
      distance / this.curSlideWidth > 1 ? 1 : distance / this.curSlideWidth
    this.traArray(this.getEle(`${this.slideClassArr}`)).forEach(
      (item, index, items) => {
        let curSildeNth = Math.abs(this.curSildeNth - index)
        let bubbleX =
          this.curSildeNth - index > 0 ? -bubbleBetween : bubbleBetween //由于透视值补充的间距值
        // let scaleIndex = bubbleNum ? curSildeNth % 2:curSildeNth
        let scaleIndex = bubbleNum
          ? curSildeNth % 2
          : glideDirect > 0
          ? index - this.curSildeNth
          : this.curSildeNth - index
        if (scaleIndex == 1) {
          // 第一个
          item.style.transform = `translate3d(${bubbleX - bubbleX * descY}px,${
            bubbleY - bubbleY * descY
          }px,0) scale(${bubbleScale + descY * (1 - bubbleScale)})`
        } else if (scaleIndex == 0) {
          item.style.transform = `translate3d(${
            glideDirect > 0 ? -(bubbleX * descY) : bubbleX * descY
          }px,${bubbleY * descY}px,0) scale(${1 - descY * (1 - bubbleScale)})`
        }
      }
    )
  }
}

/**
 * @constructor
 * @desc 居左卡片轮播
 */

function CardLeft(opt) {
  this.sign = 'cardLeft'
  this.swiperFn = function (time) {
    const leftOrigin = this.offsetVal() - this.clientWidth / 2
    const cardWidth = this.curSlideWidth
    const cardWidthscaleX = opt.cardWidthscaleX * this.curSlideWidth
    this.removeClsName(
      this.traArray(this.getEle(`${this.slideClassArr}`)),
      'swiper-slide-active'
    )
    this.traArray(this.getEle(`${this.slideClassArr}`)).forEach(
      (item, index, items) => {
        let curSildeNth = Math.abs(this.curSildeNth - index)
        item.style.transitionOrigin = '0 0'
        item.style.transitionProperty = 'width'
        item.style.transitionDuration = time + 'ms'
        item.style.width = `${curSildeNth == 0 ? cardWidthscaleX : cardWidth}px`
        if (Math.abs(this.curSildeNth - index) == 0) {
          this.setClsName(item, 'swiper-slide-active')
        }
      }
    )
    this.wrapperDom.style.perspective = `${this.perspective}px`
    this.wrapperDom.style.perspectiveOrigin = `${Math.abs(leftOrigin)}px ${
      this.perspectiveOriginY
    }`
  }
  this.touchMoveFn = function () {
    var glideDirect, fingerSlideVal, relateFingerSlide, curSlideCenterOffset
    var deep = this.touchRatio
    const cardWidth = this.curSlideWidth
    const cardWidthscaleX = opt.cardWidthscaleX * this.curSlideWidth
    ;(glideDirect = this.swiperDirectionVal()), //左边是1 ，右边是 -1
      (fingerSlideVal = this.moveX - this.preX), //手指滑动的距离
      (relateFingerSlide = deep * fingerSlideVal), //手指滑动的相对距离
      (curSlideCenterOffset = this.offsetVal()), //当前居中滑块偏移量
      (this.wrapperDom.style.transform = `translate(${
        curSlideCenterOffset + relateFingerSlide
      }px)`)
    if (
      Math.abs(relateFingerSlide) >= this.curSlideWidth &&
      this.canContinuitySlide
    ) {
      this.canSlide = false
      return false
    }
    var distance = Math.abs(relateFingerSlide)
    var descY =
      distance / this.curSlideWidth > 1 ? 1 : distance / this.curSlideWidth
    this.traArray(this.getEle(`${this.slideClassArr}`)).forEach(
      (item, index) => {
        item.style.transitionOrigin = '0 0'
        item.style.transitionProperty = 'width'
        if (glideDirect > 0) {
          if (Math.abs(this.curSildeNth - index + 1) == 0) {
            item.style.width =
              cardWidth + (cardWidthscaleX - cardWidth) * descY + 'px'
          }
          if (Math.abs(this.curSildeNth - index) == 0) {
            item.style.width =
              cardWidthscaleX - (cardWidthscaleX - cardWidth) * descY + 'px'
          }
        } else {
          if (Math.abs(this.curSildeNth - index - 1) == 0) {
            item.style.width =
              cardWidth + (cardWidthscaleX - cardWidth) * descY + 'px'
          }
          if (Math.abs(this.curSildeNth - index) == 0) {
            item.style.width =
              cardWidthscaleX - (cardWidthscaleX - cardWidth) * descY + 'px'
          }
        }
      }
    )
  }
}

/**
 * @constructor
 * @desc 渐变fade轮播
 */

function Fade() {
  this.sign = 'fade'
  this.swiperFn = function (time) {
    const leftOrigin = this.offsetVal() - this.clientWidth / 2
    this.removeClsName(
      this.traArray(this.getEle(`${this.slideClassArr}`)),
      'swiper-slide-active'
    )
    this.traArray(this.getEle(`${this.slideClassArr}`)).forEach(
      (item, index, items) => {
        item.style.transitionDuration = this.fadeSpeed+'ms'
        item.style.transitionProperty ='opacity'
        item.style.opacity = 0
        if (Math.abs(this.curSildeNth - index) == 0) {
          this.setClsName(item, 'swiper-slide-active')
          item.style.opacity = 1
        }
      }
    )
  }
  this.touchMoveFn = function () {
    var  fingerSlideVal, relateFingerSlide
    var deep = this.touchRatio
      fingerSlideVal = this.moveX - this.preX //手指滑动的距离
      relateFingerSlide = deep * fingerSlideVal //手指滑动的相对距离
    if (
      Math.abs(relateFingerSlide) >= this.curSlideWidth &&
      this.canContinuitySlide
    ) {
      this.canSlide = false
      return false
    }
  }
}

$block.cardScreen = CardScreen
$block.coverflow = Coverflow
$block.tabflow = Tabflow
$block.bubble = Bubble
$block.cardLeft = CardLeft
$block.fade = Fade

var defaultOpt = {
  loop: false, // 是否循环切换
  autoplay: false,
  pagination: {
    // 分页器
    el: '',
    elDom: null,
    indicatorActiveColor: 'rgba(255, 255, 255, 1)',
    indicatorDefultColor: 'rgba(255, 255, 255, 0.5)',
    render() {},
    update() {},
  },
  tabTitle: {
    // tab分组类型
    el: '',
    elDom: null,
    iDom: null,
    render() {},
    update() {},
  },
  thumbs: {
    // 缩略图
    el: '',
    elDom: null,
    iDom: null,
    thumbsList: [],
    height: 100,
    spaceRight: 5,
    thumbsImgDecorationSrc: '',
    render() {},
    update() {},
    setActive() {},
  },
  initialSlide: 0, //初始化下标
  direction: 'x',
  leftOriginOffset: 0,
  rightOriginOffset: 0,
  spaceBetween: 0, //滑块间距
  customOffsetY: 0, //滑块间距
  speed: 300,
  canContinuitySlide: false, //是否可以一次滑动多个滑块
  effect: '',
  touchRatio: 1,
  perspective: 500,
  perspectiveOriginY: '50%',
  slideClass: '.swiper-slide', //滑块class
  wrapperClass: '.swiper-wrapper', //包裹滑块得class
  slideToClickedSlide: false, //设置为true则点击slide会过渡到这个slide
  noSlideTouchClass: ['swiper-tabTitle', 'swiper-noSwiping'],
  rotateDeg: 0,
  lazyLoad: false, //是否懒加载图片 需要将图片img标签的src改写成data-src，并且增加类名swiper-lazy
  bubbleScale: 0.8,
  bubbleNum: 1,
  bubbleY: 0,
  cardWidthscaleX: 1.5,
  swiperTimer: {},
  updateFlag: true,
  fadeSpeed:300
}
/**
 * @constructor
 * @desc 用到的一些方法公共方法
 */
function Utils() {
  this.traArray = function (data) {
    return [].slice.call(data)
  }
  this.createDom = function (tag, length = 1) {
    var curDom = ''
    try {
      curDom = new Array(length)
        .fill('')
        .map((item) => document.createElement(tag))
    } catch (e) {
      curDom = Object.keys(
        Array.apply(null, {
          length: length,
        })
      ).map(function (item) {
        return +item
      })
    }
    return curDom
  }
  this.setClsName = function (node, clsName) {
    if (node.getAttribute('class').indexOf(clsName) != -1) return false
    node &&
      node.setAttribute('class', node.getAttribute('class') + ' ' + clsName)
  }
  this.removeClsName = function (nodeList, clsName) {
    for (var i = 0; i < nodeList.length; i++) {
      if (nodeList && nodeList[i] && nodeList[i].classList) {
        nodeList[i].classList.remove(clsName)
      } else {
        var clsList = nodeList[i].getAttribute('class').split(/\s+/)
        var tmpArr = []
        for (var j = 0; j < clsList.length; j++) {
          clsList[j] !== clsName ? tmpArr.push(clsList[j]) : false
        }
        nodeList[i].setAttribute('class', tmpArr.join(' '))
        tmpArr = null
      }
    }
  }
  this.proxyOptions = function (target, sourceKey) {
    let data = target[sourceKey]
    let keys = Object.keys(data)
    for (let i = 0, l = keys.length; i < l; i++) {
      let key = keys[i]
      Object.defineProperty(target, key, {
        enumerable: true,
        configurable: true,
        set(val) {
          this[sourceKey][key] = val
        },
        get() {
          return this[sourceKey][key]
        },
      })
    }
  }
  this.getEle = function (selector, isSingle) {
    return isSingle
      ? document.querySelector(selector)
      : document.querySelectorAll(selector)
  }
  this.setPxTovw = function (value) {
    return (value / 375) * 100 + 'vw'
  }
}

/**
 * @constructor
 * @desc 轮播核心函数
 * @param {HTMLElement} $el - 当前需要挂载swiper的元素
 * @param {} 'swiper-noSwiping' - 可以在slide上（或其他元素）增加类名'swiper-noSwiping',使该slide无法拖动
 * @param {object} opt - 用户可配置的参数
 * @param {boolean} opt.loop - 是否可以无限滚动
 * @param {number | boolean} opt.autoplay - 是否自动轮播
 * @param {object} opt.pagination - 轮播路下面的页
 * @param {number} opt.initialSlide - 初始化坐标
 * @param {string} opt.direction - Slides的滑动方向，可设置水平(x)或垂直(y) 注意：仅cardScreen类型支持
 * @param {number} opt.leftOriginOffset   -Slides的滑块距离左边的距离
 * @param {number} opt.rightOriginOffset   -Slides的滑块距离右边的距离
 * @param {number} opt.spaceBetween - 滑块的间距
 * @param {number} opt.speed - 滑动速度，即slider自动滑动开始到结束的时间
 * @param {number} opt.customOffsetY - 滑块的Y轴偏移量
 * @param {number} opt.touchRatio - 触摸距离与slide滑动距离的比例
 * @param {boolean} opt.canContinuitySlide - 是否可以一次滑动多个滑块
 * @param {boolean} opt.slideToClickedSlide - 设置为true则点击slide会过渡到这个slide。
 * @param {object} opt.on - 绑定回调事件
 * @param {object} opt.tabTitle - 轮播图版tab栏name插槽
 * @param {object} opt.thumbs - 轮播图 缩略图 插槽
 * @param {boolean | object} opt.lazyLoad [loadPrevNext:true 允许将延迟加载应用到最接近的slide的图片（前一个和后一个slide]
 *                                        - 是否懒加载图片 需要将图片img标签的src改写成data-src，并且增加类名swiper-lazy
 * @param {number} opt.effect=[cardScreen|coverflow|tabflow|bubble|cardleft] - 当前使用什么类型的轮播
 * @param {number}  opt.cardWidthscaleX  cardLeft类型激活元素放大倍数
 */

function EasySwiper($el, opt) {
  this.$el = $el
  this.$options = Object.assign({}, defaultOpt, opt)
  try {
    $block[opt.effect].call(this, this.$options)
  } catch (e) {
    console.warn('Maybe you havent set the effect yet')
  }
  Utils.call(this)
  this.clientWidth =
    (document.documentElement.clientWidth || document.body.clientWidth) -
    this.$options.leftOriginOffset -
    this.$options.rightOriginOffset
  this.leftOriginOffset = this.$options.leftOriginOffset
  this.rightOriginOffset = this.$options.rightOriginOffset

  this.direction = this.$options.direction
  this.proxyOptions(this, '$options')
  this.wrapperDom = this.getEle(this.wrapperClass)[0]
  this.slideClassArr = this.wrapperClass + ' ' + this.slideClass
  this.prex = 0
  this.moveX = 0
  this.$event = {}
  this.fadeSpeed = this.$options.speed
  this.speed = this.$options.effect == 'fade'?0:this.$options.speed 
  this.isTurnTranslation = false
  this.canSlide = true
  this.classId = `swiperTimer_${this.wrapperDom.classList[1]}`
  this.swiperTimer[this.classId] = null
  // this.proxyOptions( this.swiperTimer, times)
  var _this = this
  this.setEvent('touchstart', function (e) {
    if (
      (e.path || (e.composedPath && e.composedPath())).some(
        (item) =>
          item && item.classList && item.classList.contains('swiper-noSwiping')
      )
    ) {
      e.stopPropagation()
      return
    }
    _this.preX = 0
    _this.moveX = 0
    _this.preY = 0
    _this.moveY = 0
    _this.touchStartTime = 0
    _this.isTurnTranslation = true
    _this.canSlide = true
    _this.preX = EasySwiper.mouseDerection(e, 'x')
    _this.preY = EasySwiper.mouseDerection(e, 'y')
    _this.touchStartTime = new Date().getTime()
    _this.clearTimer(_this)
    _this.updateFlag = false

    _this.cbfunction(opt && opt.touchstart)
  })
  this.setEvent('touchmove', function (e) {
    if (
      (e.path || (e.composedPath && e.composedPath())).some(
        (item) =>
          item && item.classList && item.classList.contains('swiper-noSwiping')
      )
    ) {
      e.stopPropagation()
      return
    }
    if (!_this.canSlide && _this.canContinuitySlide) {
      return false
    }
    _this.moveX = EasySwiper.mouseDerection(e, 'x')
    _this.moveY = EasySwiper.mouseDerection(e, 'y')
    var startAxis = {
      x: _this.preX,
      y: _this.preY,
    }
    var endAxis = {
      x: _this.moveX,
      y: _this.moveY,
    }
    var isPreventDefaultEvent =
        EasySwiper.swiperSlope(startAxis, endAxis, opt.direction) <= 1 &&
        EasySwiper.swiperSlope(startAxis, endAxis, opt.direction) >= 0, //是否阻止默认事件，用来处理滑块滚动和body滚动冲突
      fingerSlideVal = _this.getDirectionY()
        ? _this.moveY - _this.preY
        : _this.moveX - _this.preX, //手指滑动的距离
      relateFingerSlide = _this.touchRatio * fingerSlideVal, //手指滑动的相对距离
      curSlideCenterOffset = _this.offsetVal() //当前居中滑块偏移量
    _this.isPreventDefaultEvent = isPreventDefaultEvent
    if (_this.isPreventDefaultEvent) {
      // 计算斜率，判断用户意图是否是滚动body还是 滚动swiper区域。
      event.preventDefault()
    } else {
      return false
    }
    if(opt.effect !== 'fade'){
      _this.wrapperDom.style.transform = _this.getDirectionY()
        ? `translate(0,${curSlideCenterOffset + relateFingerSlide}px)`
        : `translate(${curSlideCenterOffset + relateFingerSlide}px,0)` //设置父元素的偏移量
    }
      _this.touchMoveFn && _this.touchMoveFn() //执行call 过来的方法 ，这里与每一个effect类型的fn 对应
  })
  this.setEvent('touchend', function (e) {
    if (
      (e.path || (e.composedPath && e.composedPath())).some(
        (item) =>
          item && item.classList && item.classList.contains('swiper-noSwiping')
      )
    ) {
      e.stopPropagation()
      return
    }
    if (!_this.moveX) {
      return false
    }
    _this.autoPlayFn()
    _this.updateFlag = true
    var touchendTime = new Date().getTime()
    var filterDefiniteSwiper = function (val) {
      return _this.isPreventDefaultEvent &&
        touchendTime - _this.touchStartTime < _this.speed
        ? _this.swiperDirectionVal()
        : ~~val.toFixed(0)
    }
    var fingerSlideVal = _this.getDirectionY()
      ? _this.preY - _this.moveY
      : _this.preX - _this.moveX //手指滑动的距离
    var realrelateFingerSlide = _this.touchRatio * fingerSlideVal //手指滑动的相对距离
    var cardLeftwidth = opt.cardWidthscaleX * _this.curSlideWidth //cardLeft取激活数据
    var unit = _this.getDirectionY()
      ? _this.slideDom[0].offsetHeight
      : opt.effect == 'cardLeft'
      ? cardLeftwidth
      : _this.slideDom[0].offsetWidth
    var realSwiperNum =
      _this.curSildeNth + filterDefiniteSwiper(realrelateFingerSlide / unit)
    var compatibilityBothSwiper =
      realSwiperNum < 0
        ? 0
        : realSwiperNum >= _this.slideDom.length
        ? _this.slideDom.length - 1
        : realSwiperNum
    _this.curSildeNth = _this.loop ? realSwiperNum : compatibilityBothSwiper
    _this.setCurIndex(_this.curSildeNth)
    _this.swiper(opt && opt.changeSlide)
    _this.cbfunction(opt && opt.touchend)
  })
  this.setEvent('click', function (e) {
    _this.resetTimer()
    if (!_this.slideToClickedSlide) return false
    let tabindex = e.target.getAttribute('tab-index')
    var clickX = EasySwiper.mouseDerection(e, 'x') //当前点击位置的偏移量
    if (tabindex !== null) {
      // tab切换
      if (tabindex === _this.curSildeNth) return false
      _this.curSildeNth = tabindex
      //
    } else if (_this.sign == 'cardLeft') {
      // cardLeft切换方式
      let offsetWidth0 = _this.curSlideWidth * opt.cardWidthscaleX
      let offsetWidth1 = _this.curSlideWidth
      let chagSlideNum =
        Number(clickX - offsetWidth0) > 0
          ? Math.ceil((clickX - offsetWidth0) / offsetWidth1)
          : 0
      realSwiperNum = Number(_this.curSildeNth) + Number(chagSlideNum)
      _this.curSildeNth = realSwiperNum
    } else {
      // 其他切换方式
      var curSlideCenterOffset = Math.abs(_this.offsetVal()), //当前轮播图偏移量
        curswiperX = Number(_this.curswiperVal() - curSlideCenterOffset), //当前滑块的偏移量
        chagSlideNum = Math.floor(
          (clickX - curswiperX) / _this.slideDom[0].offsetWidth
        ),
        realSwiperNum = Number(_this.curSildeNth) + Number(chagSlideNum)
      _this.curSildeNth = realSwiperNum
    }
    _this.setCurIndex(_this.curSildeNth)
    _this.swiper(opt && opt.changeSlide)
  })
  this.pagination.render = function () {
    this.elDom = []
    this.elDom = _this.createDom('div', _this.slideDom.length)
    _this.traArray(this.elDom).forEach((item) => {
      item.className = 'swiper-pagination-bullet'
      // 自定义pagination样式
      item.style.background = this.indicatorDefultColor
      try {
        _this.getEle(_this.wrapperClass + ' + div', true).appendChild(item)
      } catch (e) {
        console.warn('please define pagation dom in views')
      }
    })
    return this
  }
  this.pagination.update = function () {
    _this.traArray(this.elDom).forEach((item) => {
      item.style.background = this.indicatorDefultColor
    })
    _this.removeClsName(this.elDom, 'swiper-pagination-bullet-active')
    this.elDom[_this.curIndex].style.background = this.indicatorActiveColor
    _this.setClsName(
      this.elDom[_this.curIndex],
      'swiper-pagination-bullet-active'
    )
    return this
  }
  // 跳转到当前索引
  this.swiperTo = function (num, speed) {
    if (num === _this.curSildeNth) return false
    _this.isTurnTranslation = true
    _this.curSildeNth = this.loop ? num + this.slideDom.length : num
    _this.setCurIndex(_this.curSildeNth)
    _this.swiper(opt && opt.changeSlide, speed)
  }
  // 跳转到下一条
  this.slideNext = function (speed) {
    if (_this.curSildeNth > _this.slideDom.length - 2 && !_this.loop)
      return false
    _this.isTurnTranslation = true
    _this.curSildeNth = _this.curSildeNth + 1
    _this.setCurIndex(_this.curSildeNth)
    _this.swiper(opt && opt.changeSlide, speed)
  }
  // 跳转到上一条
  this.slidePrev = function (speed) {
    if (_this.curSildeNth <= 0 && !_this.loop) return false
    _this.isTurnTranslation = true
    _this.curSildeNth = _this.curSildeNth - 1
    _this.setCurIndex(_this.curSildeNth)
    _this.swiper(opt && opt.changeSlide, speed)
  }
  // 设置tab栏用法
  this.tabTitle.render = function () {
    this.elDom = []
    this.elDom = _this.createDom('div', _this.slideDom.length)
    this.iDom = _this.createDom('i', 1)
    // 设置active标签
    _this.traArray(this.iDom).forEach((item) => {
      item.className = 'swiper-tabTitle-active'
      _this.getEle(this.el, true).appendChild(item)
    })
    // 设置普通标签
    _this.traArray(this.elDom).forEach((item, index) => {
      item.className = 'swiper-tabTitle-item swiper-noSwiping'
      item.setAttribute('tab-index', index)
      try {
        let tabTitleHtml = _this.slideDom[index].getAttribute('tabTitle')
        item.innerHTML = tabTitleHtml
        _this.getEle(this.el, true).appendChild(item)
      } catch (e) {
        console.warn('please define tabTitle dom in views')
      }
    })
    return this
  }
  this.tabTitle.update = function () {
    let offsetLeft = _this.getEle(`.swiper-tabTitle-item`)[_this.curIndex]
      .offsetLeft
    let offsetWidth = _this.getEle(`.swiper-tabTitle-item`)[_this.curIndex]
      .offsetWidth
    let translate = offsetLeft + offsetWidth / 2
    _this.getEle(
      `.swiper-tabTitle-active`
    )[0].style.transform = `translateX(${translate}px) translateX(-50%)`
    _this.removeClsName(this.elDom, 'swiper-tabTitle-item-active')
    this.elDom[_this.curIndex] &&
      _this.setClsName(
        this.elDom[_this.curIndex],
        'swiper-tabTitle-item-active swiper-noSwiping'
      )
    return this
  }
  // 缩略图处理
  this.thumbs.render = function () {
    this.elDom = []
    this.elDom = _this.createDom('div', _this.slideDom.length)
    let elDomlength = _this.slideDom.length - 1
    let height = opt.thumbs && opt.thumbs.height
    let spaceRight = opt.thumbs && opt.thumbs.spaceRight
    let thumbslist = _this.createDom('div', 1)[0]
    thumbslist.className = 'swiper-thumbs-list'
    _this.getEle(this.el, true).appendChild(thumbslist)
    _this.traArray(this.elDom).forEach((item, index) => {
      // 创建锚点图片
      let thumbsImgSrc = _this.slideDom[index].getAttribute('thumbsImgSrc')
      let thumbsImg = _this.createDom('img', 1)[0]
      let thumbsImgActiveSrc =
        _this.slideDom[index].getAttribute('thumbsImgActiveSrc') 
      console.log(thumbsImgActiveSrc)
      // 创建锚点激活图片
      let thumbsImgActive = _this.createDom('img', 1)[0]
      item.className = 'swiper-thumbs-bullet swiper-noSwiping'
      thumbsImg.className = 'swiper-thumbsImg'
      thumbsImgActive.className = 'swiper-thumbsImgActive'
      item.appendChild(thumbsImg)
      item.appendChild(thumbsImgActive)
      // 处理链接
      try {
        thumbsImg.src = thumbsImgSrc
        thumbsImgActiveSrc && (thumbsImgActive.src = thumbsImgActiveSrc)
        item.style.height = _this.setPxTovw(height)
        //最后一个元素不设置间距
        item.style.marginRight =
          index < elDomlength ? _this.setPxTovw(spaceRight) : 0
        item.style.paddingRight = index >= elDomlength ? _this.setPxTovw(12) : 0
        item.style.marginLeft = index === 0 ? _this.setPxTovw(12) : 0
        thumbslist.appendChild(item)
        // // 绑定点击事件
        item.addEventListener('click', function () {
          _this.swiperTo(index)
        })
      } catch (e) {
        console.warn('please define pagation dom in views')
      }
    })

    // 添加一个imgdom,用来标记激活样式
    this.iDom = _this.createDom('img', 1)
    thumbslist.appendChild(this.iDom[0])
    this.iDom[0].className = 'swiper-thumbs-bullet-active swiper-noSwiping'
    this.setActive()
    return this
  }
  // 缩略图更新
  this.thumbs.update = function () {
    this.setActive()
    let timer = null
    const tab = this.elDom[_this.curIndex]
    if (!tab) return false
    let tabBar = _this
      .getEle(_this.thumbs.el)[0]
      .getElementsByClassName('swiper-thumbs-list')[0]
    const { offsetLeft, offsetWidth: tabWidth } = tab
    // 处理缩略图列表自动滚动
    if (!tab) return false
    if (!tabBar) return false
    const { offsetWidth: tabBarWidth } = tabBar
    const clientWidth = _this.clientWidth
    let newLeft = offsetLeft - (clientWidth - tabWidth) / 2 // 计算当前元素居中时的滚动条距离
    const sign = tabBar.scrollLeft - newLeft > 0 ? -4 : 4 // 滚动方向
    const scrollMaxDistance = tabBar.scrollWidth - tabBarWidth // 滚动条最大滚动距离
    newLeft =
      newLeft < 0
        ? 0
        : newLeft > scrollMaxDistance
        ? scrollMaxDistance
        : newLeft // 重新精确计算.
    // 兼容
    if (
      typeof window.getComputedStyle(document.body).scrollBehavior ===
      'undefined'
    ) {
      // 如果支持scrollBehavior smooth 属性。则用原生api
      if (newLeft < 0 || ~~newLeft === ~~tabBar.scrollLeft || timer) {
        // 防止触发多次，这边业务逻辑可以用watch 监听currentIndex 优化，
        return false
      }
      timer = setInterval(() => {
        if (
          (sign === 4 && [~~newLeft].some((i) => i <= tabBar.scrollLeft)) ||
          (sign === -4 && [~~newLeft].some((i) => i >= tabBar.scrollLeft))
        ) {
          clearInterval(timer)
          tabBar.scrollLeft = newLeft
        } else {
          tabBar.scrollLeft = tabBar.scrollLeft + sign
        }
      })
    } else {
      // 原生滚动api
      tabBar.style.scrollBehavior = 'smooth'
      tabBar.scrollLeft = newLeft
    }
    return this
  }
  this.thumbs.setActive = function () {
    this.elDom.forEach((dom, i) => {
      dom.getElementsByClassName('swiper-thumbsImgActive')[0].style.display =
        i == _this.curIndex ? 'block' : 'none'
      dom.getElementsByClassName('swiper-thumbsImg')[0].style.display =
        i == _this.curIndex ? 'none' : 'block'
    })
    // 激活边框样式
    let thumbsImgDecorationSrc =
      (
        _this.slideDom[_this.curIndex] && _this.slideDom[_this.curIndex]
      ).getAttribute('thumbsImgDecorationSrc') ||
      (opt.thumbs && opt.thumbs.thumbsImgDecorationSrc)
    if (!thumbsImgDecorationSrc) return false
    // 激活缩略图距离左边位置
    let offsetLeft =
      this.elDom[_this.curIndex] && this.elDom[_this.curIndex].offsetLeft
    // 自定义缩略图高度
    let height = opt.thumbs && opt.thumbs.height
    // 设置样式
    let curthumbsDOM = this.iDom[0]
    // 获取位置不需要缩放
    curthumbsDOM.style.left = offsetLeft + 'px'
    if (curthumbsDOM.src) return false
    curthumbsDOM.src = thumbsImgDecorationSrc
    curthumbsDOM.style.height = _this.setPxTovw(height)
  }
  this.subscribeWindow()
}

/**
 * @param {object} event - touch事件event
 * @param {string} direction - 获取x 或y轴坐标
 * @return 返回当前用户触摸屏幕的X轴或Y轴坐标
 */

EasySwiper.mouseDerection = function (event, direction) {
  var adirection = direction || 'x'
  event = event || window.event
  var _direction
  if (adirection === 'x') {
    _direction =
      event.clientX ||
      event.pageX ||
      event.touches[0].clientX ||
      event.touches[0].pageX
  } else {
    _direction =
      event.clientY ||
      event.pageY ||
      event.touches[0].clientY ||
      event.touches[0].pageY
  }
  return _direction
}

/**
 * @param {string} curBlockWidth - 当前滑块在750设计稿下的值
 * @param {string} isNosuffix - 是否不需要后缀px
 * @return 返回以750设计稿为比例，在当前屏幕宽度下 curBlockVal 的取值
 */
EasySwiper.adaptaivePx = function (curBlockVal, isNosuffix) {
  const DESIGN_WIDTH = 750
  var clientWidth = document.documentElement.clientWidth
  var _suffix = isNosuffix ? 0 : 'px'
  return (curBlockVal * clientWidth) / DESIGN_WIDTH + _suffix
}
/**
 * @param {string} startAxis - 开始坐标
 * @param {string} endAxis - 结束坐标x
 * @return 返回用户滑动手机屏幕的斜率
 */
EasySwiper.swiperSlope = function (startAxis, endAxis, direction = 'x') {
  let falgx = Math.abs((endAxis.y - startAxis.y) / (endAxis.x - startAxis.x))
  let falgy = Math.abs((endAxis.x - startAxis.x) / (endAxis.y - startAxis.y))
  return direction == 'x' ? falgx : falgy
}
EasySwiper.prototype = {
  constructor: 'EasySwiper', //使则构造器指向自己。
  update: function () {
    //核心方法。更新swiper
    if (this.getEle(this.wrapperClass)[0].childNodes.length === 0) {
      return false
    }
    this.resetDefault()
    this.init()
    this.updateFlag && this.swiper()
  },
  resetDefault: function () {
    //重置方法
    this.isTurnTranslation = false
    this.timer && clearInterval(this.timer)
  },
  init: function (cb) {
    //初始化方法
    if (!this.slideDom) {
      this.slideDom = this.getEle(`${this.slideClassArr}`)
      this.curIndex = this.initialSlide
      this.wrapperDom.style.flexDirection =
        this.direction == 'y' ? 'column' : 'row'
      this.curSildeNth = this.loop
        ? this.initialSlide + this.slideDom.length
        : this.curIndex
      this.curSlideWidth = this.slideDom[0].offsetWidth
      this.curSlideHeight = this.slideDom[0].offsetHeight
      var isSetPadding = ['cardScreen'].some((item) => item === this.sign)
      this.slideDom.length > 0 &&
        this.traArray(this.slideDom).forEach((item, $index) => {
          if (this.getDirectionY()) {
            item.style[isSetPadding ? 'paddingTop' : 'marginTop'] =
              EasySwiper.adaptaivePx(this.spaceBetween / 2)
            item.style[isSetPadding ? 'paddingBottom' : 'marginBottom'] =
              EasySwiper.adaptaivePx(this.spaceBetween / 2)
          } else {
            item.style[isSetPadding ? 'paddingLeft' : 'marginLeft'] =
              EasySwiper.adaptaivePx(this.spaceBetween / 2)
            item.style[isSetPadding ? 'paddingRight' : 'marginRight'] =
              EasySwiper.adaptaivePx(this.spaceBetween / 2)
          }
          isSetPadding && (item.style.boxSizing = 'border-box')
          item.setAttribute('data-index', $index)
          this.loop &&
            (this.wrapperDom.insertBefore(
              item.cloneNode(true),
              this.slideDom[0]
            ),
            this.wrapperDom.appendChild(item.cloneNode(true)))
        })
      this.autoPlayFn()
    }
    // 分页信息
    this.pagination.el &&
      !this.pagination.elDom &&
      this.pagination.render().update()
    // tab组件信息
    this.tabTitle.el && !this.tabTitle.elDom && this.tabTitle.render().update()
    // 缩略图
    this.thumbs.el && !this.thumbs.elDom && this.thumbs.render().update()
    Object.keys(this.$event).forEach((item) => {
      this.$el.addEventListener(item, this.$event[item], { passive: false })
    })
    cb && cb()
  },
  cbfunction: function (cb) {
    cb && cb(this.curIndex)
  },
  swiper: function (cb, spd) {
    let speed = spd || this.speed
    //滑动方法
    this.swiperFn && this.swiperFn(speed)
    this.pagination.el && this.pagination.update()
    this.tabTitle.el && this.tabTitle.update()
    this.thumbs.el && this.thumbs.update()
    this.isTurnTranslation &&
      (this.wrapperDom.style.transitionDuration = speed + 'ms')
    this.wrapperDom.style.transform = this.getDirectionY()
      ? `translate(0,${this.offsetVal()}px)`
      : `translate(${this.offsetVal()}px)` //设置父元素的偏移量
    this.lazyLoad && this.loayImageFn()
    this.isTurnTranslation &&
      setTimeout(() => {
        this.limiteLoopSwiper() //判断是否滑动到最后一张图片，然后为了无缝轮播，需要切换到初始化的位置
        this.swiperFn && this.swiperFn(0)
        this.wrapperDom.style.transitionDuration = '0ms'
        this.wrapperDom.style.transform = this.getDirectionY()
          ? `translate(0,${this.offsetVal()}px)`
          : `translate(${this.offsetVal()}px)` //设置父元素的偏移量
      }, speed)
    cb && cb(this.curIndex)
  },
  // 处理懒加载图片
  loayImageFn: function () {
    // 默认懒加载当前图片  loadPrevNext:true 允许将延迟加载应用到最接近的slide的图片
    let loadPrevNext = this.lazyLoad && this.lazyLoad.loadPrevNext
    let slideDomList = this.getEle(`${this.wrapperClass} ${this.slideClass}`)
    let lazyList =
      slideDomList &&
      slideDomList[this.curSildeNth] &&
      slideDomList[this.curSildeNth].querySelectorAll('.swiper-lazy')
    if (loadPrevNext) {
      // 许将延迟加载应用到最接近的slide的图片
      let _lazyList1 =
        slideDomList &&
        slideDomList[this.curSildeNth + 1] &&
        slideDomList[this.curSildeNth + 1].querySelectorAll('.swiper-lazy')
      let _lazyList2 =
        slideDomList &&
        slideDomList[this.curSildeNth - 1] &&
        slideDomList[this.curSildeNth - 1].querySelectorAll('.swiper-lazy')
      lazyList = [...lazyList, ..._lazyList1, ..._lazyList2]
    }
    lazyList &&
      lazyList.forEach((lazyImg) => {
        if (lazyImg.getAttribute('src')) return false
        lazyImg && lazyImg.setAttribute('src', lazyImg.getAttribute('data-src'))
      })
  },
  clearTimer: function (e) {
    if (this.swiperTimer[this.classId]) {
      clearInterval(this.swiperTimer[this.classId])
      this.swiperTimer[this.classId] = null
    }
  },
  resetTimer: function (e) {
    this.clearTimer()
    this.updateFlag = false
    this.autoPlayFn()
    this.updateFlag = true
  },
  autoPlayFn: function () {
    //自动滚动
    if (!this.autoplay) {
      return false
    }
    var speedautoplay =
      typeof this.autoplay === 'boolean' ? 3000 : this.autoplay
    if (this.swiperTimer[this.classId]) return false
    this.swiperTimer[this.classId] = setInterval(() => {
      this.isTurnTranslation = true
      // 循环处理下标
      if (
        this.loop ||
        this.curSildeNth <
          this.getEle(`${this.wrapperClass} ${this.slideClass}`).length - 1
      ) {
        this.curSildeNth++
      } else {
        this.curSildeNth = 0
      }
      this.setCurIndex(this.curSildeNth)
      this.swiper(this.$options && this.$options.changeSlide)
    }, speedautoplay)
  },
  setEvent: function (name, fn) {
    //建立touch 事件。
    this.$event[name] = fn
  },
  swiperDirectionVal: function () {
    // 判断用户滑动方向的值
    let falgX = this.preX - this.moveX <= 0 ? -1 : 1
    let falgY = this.preY - this.moveY <= 0 ? -1 : 1
    return this.direction == 'y' ? falgY : falgX
  },
  subscribeWindow: function () {
    //监听浏览器窗口变化，初始化
    const _this = this
    window.addEventListener('resize', function (e) {
      _this.clientWidth =
        document.documentElement.clientWidth || document.body.clientWidth
      _this.curSlideWidth = _this.getEle(
        `${this.slideClassArr}`,
        true
      ).offsetWidth
      _this.update()
    })
  },
  curswiperVal: function () {
    let offsetLeft =
      this.getEle(`${this.wrapperClass} ${this.slideClass}`)[
        this.curSildeNth
      ] &&
      this.getEle(`${this.wrapperClass} ${this.slideClass}`)[this.curSildeNth]
        .offsetLeft
    return offsetLeft
  },
  offsetVal: function () {
    let offsetVal = 0
    // 计算当前滑块距离，使之一开始就呈居中的状态。
    if (this.sign == 'cardLeft') {
      // cardleft类型，激活居左  计算当前滑块距离，使之一开始就居左
      let offsetcardleft =
        this.getEle(`${this.wrapperClass} ${this.slideClass}`)[
          this.curSildeNth - 1
        ] &&
        this.getEle(`${this.wrapperClass} ${this.slideClass}`)[
          this.curSildeNth - 1
        ].offsetLeft
      offsetVal = -(offsetcardleft + this.curSlideWidth - this.leftOriginOffset)
    } else if (this.direction == 'y') {
      let offsetTop =
        this.getEle(`${this.wrapperClass} ${this.slideClass}`)[
          this.curSildeNth
        ] &&
        this.getEle(`${this.wrapperClass} ${this.slideClass}`)[this.curSildeNth]
          .offsetTop
      offsetVal = -offsetTop
    } else {
      // 计算当前滑块距离，使之一开始就呈居中的状态。
      let offsetLeft =
        this.getEle(`${this.wrapperClass} ${this.slideClass}`)[
          this.curSildeNth
        ] &&
        this.getEle(`${this.wrapperClass} ${this.slideClass}`)[this.curSildeNth]
          .offsetLeft
      offsetVal = -offsetLeft + (this.clientWidth - this.curSlideWidth) / 2
    }
    return offsetVal
  },
  destroy: function () {
    //卸载方法
    Object.keys(this.$event).forEach((item) => {
      this.$el.removeEventListener(item, this.$event[item])
      this.clearTimer()
    })
  },
  limiteLoopSwiper: function () {
    //针对无限滚动做的判断,如果滚动到clone的dom，则归为index,使则可以无限滚动
    if (this.loop) {
      if (this.curSildeNth >= this.slideDom.length * 2) {
        this.curSildeNth = this.curSildeNth - this.slideDom.length
      } else if (this.curSildeNth < this.slideDom.length) {
        this.curSildeNth = this.curSildeNth + this.slideDom.length
      }
    }
  },
  setCurIndex: function (curSildeNth) {
    this.curIndex =
      this.getEle(`${this.slideClassArr}`) &&
      this.getEle(`${this.slideClassArr}`)[curSildeNth] &&
      this.getEle(`${this.slideClassArr}`)[curSildeNth].getAttribute(
        'data-index'
      )
  },
  // 仅全屏轮播支持 纵向滚动
  getDirectionY: function () {
    return this.sign == 'cardScreen' && this.direction == 'y'
  },
}

/* harmony default export */ var package_EasySwiper = (EasySwiper);

// EXTERNAL MODULE: ./src/assets/style/index.css
var style = __webpack_require__("2046");

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./src/package/Swiper/index.vue?vue&type=script&lang=js&


function __adaptaivePx (curBlockWidth, isNosuffix) {
  const DESIGN_WIDTH = 750
  const clientWidth = document.documentElement.clientWidth
  const _suffix = isNosuffix ? 0 : 'px'
  return String(curBlockWidth) * clientWidth / DESIGN_WIDTH + _suffix
}

function isNum (val) {
  return /^\d+$/g.test(val)
}

function __setVal (val) {
  if (!isNum(val)) return val
  return __adaptaivePx(val)
}

const DEFAULT_WRAPPER_STYLE_SETTING = {
  'cardScreen': {
    width: '100%',
    height: 'auto',
  },
  'coverflow': {
    width: '100%',
    height: 'auto',
    distanceTop: 0,
    distanceBtm: 0,
  },
  'tabflow': {
    width: '100%',
    height: 'auto',
  },
  'cardLeft': {
    width: '100%',
    height: 'auto',
  },
}


/* harmony default export */ var Swipervue_type_script_lang_js_ = ({
  name: 'Swiper',
  data () {
    return {
      classes: {
        wrapperClass: 'swiper-wrapper'
      },
      swiper: null,
      ranStr: '',
      wrapStyle: {}
    }
  },
  props: {
    height: {
      default: '',
      type: [String, Number]
    },
    width: {
      default: '',
      type: [String, Number]
    },
    options: {
      default: () => ({}),
      type: Object
    },
    distanceTop: {
      default: 0,
      type: [String, Number]
    },
    distanceBtm: {
      default: 0,
      type: [String, Number]
    },
    background: {
      default: '',
      type: String
    },
  },
  computed: {
    wrapClass () {
      return this.options.wrapperClass.slice(1)
    },
  },
  activated () {
    this.update()
  },
  mounted () {
    this.ranStr = this.classes.wrapperClass + Math.random().toString(36).substring(4)
    this.createSwiperStyle()
    let optionClone = JSON.parse(JSON.stringify(this.options))
    this.$nextTick(() => {
      this.swiper = new package_EasySwiper(this.$el, { 
        ...optionClone,
        wrapperClass: '.' + this.ranStr,
        changeSlide: (index) => this.$emit('changeSlide', index),
        touchstart: (index) => this.$emit('touchStartSwiper', index),
        touchend: (index) => this.$emit('touchEndSwiper', index),
      })
    })
  },
  updated () {
    this.$nextTick(() => {
      this.update()
    })
  },
  watch: {
  },
  beforeDestroy () {
    this.$nextTick(() => {
      if (this.swiper) {
        this.swiper.destroy && this.swiper.destroy()
        delete this.swiper
      }
    })
  },
  methods: {
    update () {
      this.swiper && this.swiper.update && this.swiper.update()
    },
    createSwiperStyle () {
      const effectDefaultHeight = this.height || DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect] && DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect].height
      const effectDefaultWidth = this.width || DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect] && DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect].width
      const effectDefaultDistanceTop = this.distanceTop || DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect] && DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect].distanceTop
      const effectDefaultDistanceBtm = this.distanceBtm || DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect] && DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect].distanceBtm
      const effectDefaultBackground = this.background || DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect] && DEFAULT_WRAPPER_STYLE_SETTING[this.options.effect].background
      this.wrapStyle = {
        height: __setVal(effectDefaultHeight),
        width: __setVal(effectDefaultWidth),
        paddingTop: __setVal(effectDefaultDistanceTop),
        paddingBottom: __setVal(effectDefaultDistanceBtm),
        background: effectDefaultBackground
      }
    }
  }
});

// CONCATENATED MODULE: ./src/package/Swiper/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var package_Swipervue_type_script_lang_js_ = (Swipervue_type_script_lang_js_); 
// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent(
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */,
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options =
    typeof scriptExports === 'function' ? scriptExports.options : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) {
    // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
          injectStyles.call(
            this,
            (options.functional ? this.parent : this).$root.$options.shadowRoot
          )
        }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/package/Swiper/index.vue





/* normalize component */

var component = normalizeComponent(
  package_Swipervue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Swiper = (component.exports);
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"0b6073b3-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??ref--5!./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./src/package/SwiperItem/index.vue?vue&type=template&id=0647ed60&
var SwiperItemvue_type_template_id_0647ed60_render = function render(){var _vm=this,_c=_vm._self._c;return _c('div',{class:_vm.slideClass,style:(_vm.slideStyle)},[_vm._t("default")],2)
}
var SwiperItemvue_type_template_id_0647ed60_staticRenderFns = []


// CONCATENATED MODULE: ./src/package/SwiperItem/index.vue?vue&type=template&id=0647ed60&

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--1-0!./node_modules/vue-loader/lib??vue-loader-options!./src/package/SwiperItem/index.vue?vue&type=script&lang=js&

function SwiperItemvue_type_script_lang_js_adaptaivePx(curBlockWidth, isNosuffix) {
  const DESIGN_WIDTH = 750;
  var clientWidth = document.documentElement.clientWidth;
  var _suffix = isNosuffix ? 0 : "px";
  return (curBlockWidth * clientWidth) / DESIGN_WIDTH + _suffix;
}

function SwiperItemvue_type_script_lang_js_isNum(val) {
  // return Object.prototype.toString.call(val) === '[object Number]'
  return /^\d+$/g.test(val);
}

function SwiperItemvue_type_script_lang_js_setVal(val) {
  if (!SwiperItemvue_type_script_lang_js_isNum(val)) return val;
  return SwiperItemvue_type_script_lang_js_adaptaivePx(val);
}

const DEFAULT_SLIDE_STYLE_SETTING = {
  cardScreen: {
    width: "100%",
    height: "auto",
  },
  coverflow: {
    width: 300,
    height: 'auto',
  },
  cardLeft: {
    width: 200,
    height: 'auto',
  },
};
/* harmony default export */ var SwiperItemvue_type_script_lang_js_ = ({
  name: "SwiperItem",
  data() {
    return {
      slideClass: "swiper-slide",
    };
  },
  props: {
    height: {
      default: "",
      type: [String, Number],
    },
    width: {
      default: "",
      type: [String, Number],
    },
  }, 
  computed:{
    effectDefaultHeight(){
      return this.height || DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect] && DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect].height
    },
    effectDefaultWidth(){
      return  this.width || DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect] && DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect].width 
    },
    slideStyle() {
      const {effectDefaultHeight, effectDefaultWidth} = this
      return {
        height:SwiperItemvue_type_script_lang_js_setVal(effectDefaultHeight),
        width: SwiperItemvue_type_script_lang_js_setVal(effectDefaultWidth), 
      }
    },
  },
  mounted() {
    // 关闭this.createSlideStyle() 用computed计算 元素的样式需在DOM执行完成后立马执行计算，避免3d轮播图中初始化宽度计算错误
    // this.createSlideStyle();
    // this.update();
  },
  updated() {
    this.update();
  },
  attached() {
    this.update();
  },
  methods: {
    update() {
      if (this.$parent && this.$parent.swiper) {
        this.$parent.update();
      }
    },
    createSlideStyle() {
      const effectDefaultHeight =
        this.height ||
        (DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect] &&
          DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect].height);
      const effectDefaultWidth =
        this.width ||
        (DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect] &&
          DEFAULT_SLIDE_STYLE_SETTING[this.$parent.options.effect].width);
      this.slideStyle = {
        height: SwiperItemvue_type_script_lang_js_setVal(effectDefaultHeight),
        width: SwiperItemvue_type_script_lang_js_setVal(effectDefaultWidth),
      };
    },
  },
});

// CONCATENATED MODULE: ./src/package/SwiperItem/index.vue?vue&type=script&lang=js&
 /* harmony default export */ var package_SwiperItemvue_type_script_lang_js_ = (SwiperItemvue_type_script_lang_js_); 
// CONCATENATED MODULE: ./src/package/SwiperItem/index.vue





/* normalize component */

var SwiperItem_component = normalizeComponent(
  package_SwiperItemvue_type_script_lang_js_,
  SwiperItemvue_type_template_id_0647ed60_render,
  SwiperItemvue_type_template_id_0647ed60_staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var SwiperItem = (SwiperItem_component.exports);
// CONCATENATED MODULE: ./src/package/index.js
//package/index.js
 // 引入封装好的组件
 // 引入封装好的组件

const coms = [Swiper,SwiperItem]; // 将来如果有其它组件,都可以写到这个数组里

// 批量组件注册
const install = function (Vue) {
  coms.forEach((com) => {
    Vue.component(com.name, com);
  });
};

/* harmony default export */ var src_package = (install); // 这个方法以后再使用的时候可以被use调用

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (src_package);



/***/ })

/******/ });
});