# easy-swiper-new

# 简易 swiper 组件

# 使用方法

### install

- npm install easy-swiper-new --save

- yarn add easy-swiper-new

### Registration

    import Vue from 'vue'
    import Swiper from "easy-swiper-new";
    import swiperItem from "easy-swiper-new";
    import 'easy-swiper-new/easy-swiper-new.css'
    Vue.use(Swiper).use(swiperItem)

### Properties

| Prop                | Type                           |                          Description                           |   Default    |
| :------------------ | :----------------------------- | :------------------------------------------------------------: | :----------: |
| loop                | boolean                        |                        是否可以无限滚动                        |    false     |
| autoplay            | number/boolean                 |                          是否自动轮播                          |    false     |
| direction           | string                         |     轮播图滑动方向(水平(x)或垂直(y))仅 cardScreen 类型支持     |      x       |
| initialSlide        | number                         |                         轮播图默认下标                         |      0       |
| spaceBetween        | number                         |                           滑块的间距                           |      0       |
| speed               | number                         |          滑动速度，即 slider 自动滑动开始到结束的时间          |    300ms     |
| customOffsetY       | number                         |                       滑块的 Y 轴偏移量                        |      0       |
| touchRatio          | number                         |                触摸距离与 slide 滑动距离的比例                 |      1       |
| canContinuitySlide  | boolean                        |                    是否可以一次滑动多个滑块                    |     true     |
| slideToClickedSlide | boolean                        |         设置为 true 则点击 slide 会过渡到这个 slide。          |    false     |
| perspective         | number                         |                      3D 元素距视图的距离                       |     500      |
| perspectiveOriginY  | number                         |            3D 元素所基于的 Y 轴 透视效果(默认居中)             |     50%      |
| rotateDeg           | number                         |                        3D 元素旋转角度                         |   0(1.1.0)   |
| leftOriginOffset    | number                         |                      轮播图距离左边的距离                      |      0       |
| rightOriginOffset   | number                         |                      轮播图距离右边的距离                      |      0       |
| lazyLoad            | boolean/object                 |        是否懒加载(loadPrevNext:true 可延迟加载前后图片)        | false(1.1.0) |
| tabTitle            | object                         |                  轮播图 tab 栏类型 name 插槽                   |              |
| effect              | string                         | 当前使用轮播类型(cardScreen/coverflow/tabflow/bubble/cardLeft/fade) |  cardScreen  |
| pagination          | object                         |                         轮播路下面的页                         |              |
| pagination 配置     | indicatorActiveColor           |                          激活页数颜色                          |
| pagination 配置     | indicatorDefultColor           |                         未激活页数颜色                         |
| thumbs              | object                         |             轮播图缩略图插槽（图片取值 thumbsImg）             |   (1.1.0)    |
| thumbs 插槽配置     | height(number)                 |                              高度                              |     100      |
| thumbs 插槽配置     | spaceRight(number)             |                            单个居右                            |      10      |
| thumbs 插槽配置     | thumbsImgSrc(string)           |                          小图样式图片                          |      ''      |
| thumbs 插槽配置     | thumbsImgActiveSrc(string)     |                        小图激活样式图片                        |      ''      |
| thumbs 插槽配置     | thumbsImgDecorationSrc(string) |                      小图激活边框样式图片                      |      ''      |

| bubbleNum | number(1,0) |泡泡缩放类型 |1
| bubbleScale | number |泡泡缩小比例 |0.8
| bubbleY | number |泡泡 Y 轴下移间距 |0
| bubbleBetween | number |泡泡缩小后产生的间距补贴 |0
| cardWidthscaleX | number |cardLeft 模式下激活卡片放大倍数 |1.5

### Functions

| Name      | Type       |               Description                |
| :-------- | :--------- | :--------------------------------------: |
| swiperTo  | num, speed | 跳转到指定下标位置(speed 可选，切换速度) |
| slideNext | speed      |  滑动到下一个滑块(speed 可选，切换速度)  |
| slidePrev | speed      |  滑动到前一个滑块(speed 可选，切换速度)  |

### Callbacks

`@changeSlide="changeSlide" swiper从一个slide过渡到另一个slide结束时执行，回调接收当前swiper的下标`

### Component

    <template>
        <Swiper
        :options="swiperOption"
        ref="mySwiper"
        @changeSlide="changeSlide"
        >
        <SwiperItem
            v-for="(item, index) in list"
            :width="140"
            :key="item.img"
        >
            <img :src="item.img" alt="" />
            <span>{{ index }}</span>
        </SwiperItem>
        </Swiper>
    </template>
    <script>
    export default {
        name: 'mySwiper',
        data() {
        return {
            list:[],
            swiperOption: {
                loop: true, // 是否循环切换
                initialSlide: 0,
                canContinuitySlide: false,
                spaceBetween: 20,
                effect: "cardScreen",
                pagination: {
                  el: '.swiper-pagination'
                },
                thumbs:{
                  el: '.swiper-thumbs'
                }
                // Some Swiper option/callback...
            }
        }
        },
        computed: {
            swiper() {
                return this.$refs.mySwiper.swiper
            }
        },
    }

  </script>
