import Vue from 'vue'
import App from './App.vue'

import router from './router'

// import Swiper from "easy-swiper-new";    
// import swiperItem from "easy-swiper-new";   
// import "../node_modules/easy-swiper-new/easy-swiper-new.css";  
// Vue.use(Swiper).use(swiperItem)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router:router
}).$mount('#app')


