//引入vue
import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router)
import Screen  from '../views/screen.vue'
import Coverflow  from '../views/coverflow.vue';
import Tab  from '../views/tab.vue';
import Pagination  from '../views/pagination.vue';
import Thumbs  from '../views/thumbs.vue';
import Bubble  from '../views/bubble.vue';
import Card  from '../views/card.vue';
import Fade  from '../views/fade.vue';


//定义routes路由的集合，数组类型
const routes=[
    //单个路由均为对象类型，path代表的是路径，component代表组件
    {path:'/screen',component:Screen},
    {path:'/coverflow',component:Coverflow},
    {path:'/tab',component:Tab},
    {path:'/pagination',component:Pagination},
    {path:'/thumbs',component:Thumbs},
    {path:'/bubble',component:Bubble},
    {path:'/card',component:Card},
    {path:'/fade',component:Fade},
    
]


export default new Router({
  mode: 'history',
  routes
})

//抛出这个这个实例对象方便外部读取以及访问
// export default router